[package]
name = "conduit"
description = "A Matrix homeserver written in Rust"
license = "Apache-2.0"
authors = ["timokoesters <timo@koesters.xyz>"]
homepage = "https://conduit.rs"
repository = "https://gitlab.com/famedly/conduit"
readme = "README.md"
version = "0.1.0"
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
# Used to handle requests
# TODO: This can become optional as soon as proper configs are supported
#rocket = { git = "https://github.com/SergioBenitez/Rocket.git", rev = "8d779caa22c63b15a6c3ceb75d8f6d4971b2eb67", default-features = false, features = ["tls"] } # Used to handle requests
rocket = { git = "https://github.com/timokoesters/Rocket.git", branch = "empty_parameters", default-features = false, features = ["tls"] }

# Used for matrix spec type definitions and helpers
#ruma = { git = "https://github.com/ruma/ruma", features = ["rand", "client-api", "federation-api", "unstable-pre-spec", "unstable-synapse-quirks"], rev = "aff914050eb297bd82b8aafb12158c88a9e480e1" }
ruma = { git = "https://github.com/timokoesters/ruma", features = ["rand", "client-api", "federation-api", "unstable-exhaustive-types", "unstable-pre-spec", "unstable-synapse-quirks"], branch = "timo-fed-fixes" }
#ruma = { path = "../ruma/ruma", features = ["unstable-exhaustive-types", "rand", "client-api", "federation-api", "unstable-pre-spec", "unstable-synapse-quirks"] }

# Used when doing state resolution
state-res = { git = "https://github.com/timokoesters/state-res", branch = "spec-comp", features = ["unstable-pre-spec"] }
#state-res = { path = "../state-res", features = ["unstable-pre-spec"] }

# Used for long polling
tokio = "0.2.22"
# Used for storing data permanently
sled = { version = "0.34.4", default-features = false }
# Used for emitting log entries
log = "0.4.11"
# Used for rocket<->ruma conversions
http = "0.2.1"
# Used to find data directory for default db path
directories = "3.0.1"
# Used for number types for ruma
js_int = "0.1.9"
# Used for ruma wrapper
serde_json = { version = "1.0.57", features = ["raw_value"] }
# Used for pdu definition
serde = "1.0.116"
# Used for secure identifiers
rand = "0.7.3"
# Used to hash passwords
rust-argon2 = "0.8.2"
# Used to send requests
reqwest = "0.10.8"
# Used for conduit::Error type
thiserror = "1.0.20"
# Used to generate thumbnails for images
image = { version = "0.23.9", default-features = false, features = ["jpeg", "png", "gif"] }
# Used to encode server public key
base64 = "0.12.3"
# Used when hashing the state
ring = "0.16.15"
# Used when querying the SRV record of other servers
trust-dns-resolver = "0.19.5"

[features]
default = ["conduit_bin"]
conduit_bin = [] # TODO: add rocket to this when it is optional
tls_vendored = ["reqwest/native-tls-vendored"]

[[bin]]
name = "conduit"
path = "src/main.rs"
required-features = ["conduit_bin"]

[lib]
name = "conduit"
path = "src/lib.rs"

[package.metadata.deb]
name = "matrix-conduit"
maintainer = "Paul van Tilburg <paul@luon.net>"
copyright = "2020, Timo Kösters <timo@koesters.xyz>"
license-file = ["LICENSE", "3"]
depends = "$auto, ca-certificates"
extended-description = """\
A fast Matrix homeserver that is optimized for smaller, personal servers, \
instead of a server that has high scalability."""
section = "net"
priority = "optional"
assets = [
  ["debian/env.local", "etc/matrix-conduit/local", "644"],
  ["debian/README.Debian", "usr/share/doc/matrix-conduit/", "644"],
  ["README.md", "usr/share/doc/matrix-conduit/", "644"],
  ["target/release/conduit", "usr/sbin/matrix-conduit", "755"],
]
conf-files = [
  "/etc/matrix-conduit/local"
]
maintainer-scripts = "debian/"
systemd-units = { unit-name = "matrix-conduit" }
